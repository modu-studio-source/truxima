$(window).on("load resize", function () {
	$('body').attr('data-mobile',
		(function(){
			var r = ($(window).width() <= 1000) ? true : false;
			return r;
		}())
	);
}).resize();

$(function() {
	var gnb = $('#gnb');
	var gnbState = 0;

	$(window).resize(function(){
		if ($('body').attr('data-mobile') == 'false'){
			$(gnb).removeClass('js-open-m').find("li").removeClass('js-open-m');
		} else {
			$(gnb).removeClass('js-open-d').find("li").removeClass('js-open-d');
		}
	});

	$(gnb.selector+">ul>li>a").on("click mouseenter focus touchstart",function(e) {
		if ($('body').attr('data-mobile') == 'false') {
			$('.gnb__depth1-item').addClass('js-open-d');
			$(gnb).addClass('js-open-d');
			$('.main-search').removeClass('is-active');
			// if (!$(this).parents("li").hasClass("js-open-d")) {
			// 	$(gnb).addClass('js-open-d');
			// 	$(this).parents("li").addClass("js-open-d").siblings("li").removeClass("js-open-d js-first");
			// 	if (gnbState === 0) {
			// 		$(this).parents("li").addClass("js-first");
			// 		gnbState = 1;
			// 	}
			// } else {
			// 	$(gnb).removeClass('js-open-d');
			// 	$(this).parents("li").removeClass("js-open-d");
			// }
			// if(e.type == "touchstart") {
			// 	return false;
			// }
		} else {
			if(e.type == "click"){
				e.preventDefault();
				if ($(this).parents("li").hasClass("js-open-m")) {
					$(this).parents("li").removeClass("js-open-m");
				} else {
					$(this).parents("li").siblings().removeClass("js-open-m");
					$(this).parents("li").addClass("js-open-m");
				}
			}
		}
	});
	$(gnb).on("mouseleave",function() {
		$(this).removeClass('js-open-d');
		//$(gnb).find("[class$=tit]").stop().animate({height:'0'},300);
		$(this).find('li').removeClass('js-open-d js-first');
		gnbState = 0;
	});
	$(gnb).find("a").last().on("blur",function() {
		$(gnb).trigger("mouseleave");
	});
	$(gnb).find("[class$=tit]").on("click" ,function(){
		if ($('body').attr('data-mobile') == "false") return false;
		if (!$(gnb).hasClass("js-open-m")) {
			$(gnb).addClass('js-open-m');
		} else {
			$(gnb).removeClass('js-open-m');
		}
		$('.top-logo').addClass('is-hidden');
	});
	$(gnb).find('.gnb__depth2-link').on("click" ,function(e){
		if ($('body').attr('data-mobile') == "false") return true;
		var gnbClass = $(this).attr('class');
		var gnbClass2 = gnbClass.split(' ');
		if ($('body').attr('data-mobile') == "false") return true;
		if (gnbClass2[1] == "disabled") return true;
		if(e.type == "click"){
			e.preventDefault();
			if ($(this).parents(".gnb__depth2-item").hasClass("js-open-m")) {
				$(this).parents(".gnb__depth2-item").removeClass("js-open-m");
			} else {
				$(this).parents(".gnb__depth2-item").siblings().removeClass("js-open-m");
				$(this).parents(".gnb__depth2-item").addClass("js-open-m");
			}
		}
	});
	$('.gnb__menu-close').on('click',function(e){
		e.preventDefault();
		$(gnb).removeClass('js-open-m');
		$('.top-logo').removeClass('is-hidden');
	});
	$(window).on("scroll", function(e) {
		if ($('body').attr('data-mobile') == "false") return false;
		var wrap = $(gnb).parent();
		if ($(this).scrollTop() > $(wrap).height()) {
			$(wrap).addClass("js-fixed");
			$("#familySite").removeClass("js-open-m");
		} else {
			$(wrap).removeClass("js-fixed");
		}
	});
});

// $(document).ready(function() {
// 	var familySite = $("#familySite");
//
// 	$(familySite).find("[class$=list]").on("click", function(){
// 		if ($('body').attr('data-mobile') == "false") return false;
// 		if ($(familySite).hasClass("js-open-m")) {
// 			$(familySite).removeClass("js-open-m");
// 		} else {
// 			$(familySite).addClass("js-open-m");
// 		}
// 	});
// 	$(window).resize(function(){
// 		if ($('body').attr('data-mobile') == 'false'){
// 			$(familySite).removeClass("js-open-m");
// 		}
// 	});
// });


//input:file
$(document).ready(function() {
	var fileTarget = $('.upload-hidden');

	fileTarget.on('change', function() {
		if (window.FileReader) {
			// 파일명 추출
			var filename = $(this)[0].files[0].name;
		} else {
			// Old IE 파일명 추출
			var filename = $(this).val().split('/').pop().split('\\').pop();
		};

		$(this).parent().siblings('.upload-name').val(filename);
	});

	//preview image
	var imgTarget = $('.upload-hidden');

	imgTarget.on('change', function() {
		var parent = $(this).parent().parent();
		parent.children('.upload-display').remove().end().removeClass('upload-set-v1_thumb');

		if (window.FileReader) {
			//image 파일만
			if (!$(this)[0].files[0].type.match(/image\//)) return;

			var reader = new FileReader();
			reader.onload = function(e) {
				var src = e.target.result;
				parent.prepend('<span class="upload-display"><img src="' + src + '" class="upload-thumb"></span>').addClass('upload-set-v1_thumb');
			}
			reader.readAsDataURL($(this)[0].files[0]);
		} else {
			//$(this)[0].select();
			//$(this)[0].blur();
			//var imgSrc = document.selection.createRange().text;
			//parent.prepend('<span class="upload-display"><img class="upload-thumb"></span>').addClass('upload-set-v1_thumb');

			//var img = $(this).parent().siblings('.upload-display').find('img');
			//img[0].style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(enable='true',sizingMethod='scale',src=\"" + imgSrc + "\")";
		}
	});

});

//tab
$(function() {
	var tab = $("[data-tab='true']");
	var hash = window.location.hash;
	var tt= [];

	$(tab).find("a").each(function(idx){
		var t = $(tab).find("a").eq(idx).attr("href").split('#')[1];
		tt.push(t);

		if (hash) {
			sele($(tab).find("a[href="+hash+"]"));
			for (var i in tt) {
				$("#"+tt[i]).removeClass("is-active");
			}
			$(hash).addClass("is-active");
		}

		$(this).on("click", function(e){
			if (this.href.match(/#([^ ]*)/g)) {
				e.preventDefault();
				if (!$(this).parent().hasClass("is-active")) window.location.hash = ($(this).attr("href"));
				sele($(this));
				for (var i in tt) {
					$("#"+tt[i]).removeClass("is-active");
				}
				$("#"+t).addClass("is-active");
			}
		});



	})
	if ($(tab).hasClass("tab-v1")) {
		$(tab).find("[class$=list]").on("click", function(){
			//console.log($('body').attr('data-mobile'));
			if ($('body').attr('data-mobile') == 'true'){
				if ($(this).hasClass("js-open-m")) {
					$(this).removeClass("js-open-m");
				} else {
					$(this).addClass("js-open-m");
				}
				if($(this).parents('.content__wrap').hasClass('is-active')){
					$(this).parents('.content__wrap').removeClass("is-active");
				} else {
					$(this).parents('.content__wrap').addClass("is-active");
				}
			}
		});
		$(window).resize(function(){
			if ($('body').attr('data-mobile') == 'false') $(".tab-v1").removeClass("js-open-m");
		});
	}

	function sele(el) {
		$(el).parent().addClass("is-active").siblings().removeClass("is-active");
	}

});
$(function() {
	$('.snb__link').on('click',function(e){
		var snbClass = $(this).attr('class');
		var snbClass2 = snbClass.split(' ');
		if (snbClass2[1] == "disabled") return true;
		e.preventDefault();
		$(this).parents('.snb__item').addClass('is-active').siblings().removeClass('is-active');
	});
});

$(document).ready(function(){
	$(".select__box").click(function(){
		$(".select__menu").toggleClass("is-active");
		$(".select__item").click(function(){
			$(".select__box > p").text($(this).text());
			$(".select__menu").removeClass("is-active");
		});
	});
	//검색
	$('.js-search-open').on('click',function(e){
		e.preventDefault();
		$('.main-search').addClass('is-active');
		$('.gnb, .gnb__depth1-item').removeClass('js-open-d');
	});
	$('.js-search-close').on('click',function(e){
		e.preventDefault();
		$('.main-search').removeClass('is-active');
	});
	//팝업
	$('.js-open-popup').on('click',function(e){
		e.preventDefault();
		$('.popup, .dimmed').show();
	});
	$('.popup__close').on('click',function(e){
		e.preventDefault();
		$('.popup, .dimmed').hide();
	});
});
