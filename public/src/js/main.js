// $("#touchSlider6").touchSlider({
// 	view : 4,
// 	initComplete : function (e) {
// 		var _this = this;
// 		var $this = $(this);
// 		var paging = $this.next(".paging");
// 		var len = Math.ceil(this._len / this._view);
//
// 		paging.html("");
// 		for(var i = 1; i <= len; i++) {
// 			paging.append('<button type="button" class="btn_page">page' + i + '</button>');
// 		}
//
// 		paging.find(".btn_page").bind("click", function (e) {
// 			_this.go_page($(this).index());
// 		});
// 	},
// 	counter : function (e) {
// 		$(this).next(".paging").find(".btn_page").removeClass("on").eq(e.current-1).addClass("on");
// 	}
// });

$(window).on("resize", function () {
    var multiView = ($(window).width() <= 1023) ? 1 : 4 ;
    $('#touchSlider6').touchSlider({
        roll : true,
        view : multiView,
        resize : true,
		transition: true,
		sidePage : true,
		initComplete : function (e) {
			var _this = this;
			var $this = $(this);
			var paging = $this.next(".paging");
			var len = Math.ceil(this._len / this._view);

			paging.html("");
			for(var i = 1; i <= len; i++) {
				paging.append('<button type="button" class="btn_page">page' + i + '</button>');
			}

			paging.find(".btn_page").bind("click", function (e) {
				_this.go_page($(this).index());
			});
		},
		counter : function (e) {
			$(this).next(".paging").find(".btn_page").removeClass("on").eq(e.current-1).addClass("on");
		}
    });
}).resize();
$('.disease__btn').on('mouseenter',function(){
	$(this).parents('.disease__item').addClass('on')
}).on('mouseleave',function(){
	$(this).parents('.disease__item').removeClass('on')
});
$('.news__list').cycle({
	slides : '.news__item',
	fx : 'scrollHorz',
	prev : '.news__btn-prev',
	next:'.news__btn-next'
});
// var news1 = $( '.news__list' ).bxSlider( {
//     mode: 'horizontal',
// 	controls: false ,
// 	pager:false
// } );
// $( '.news__btn-prev' ).on( 'click', function (e) {
// 	e.preventDefault();
// 	news1.goToPrevSlide();
// } );
// $( '.news__btn-next' ).on( 'click', function (e) {
// 	e.preventDefault();
// 	news1.goToNextSlide()();
// } );
